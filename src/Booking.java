import java.util.Date;

public class Booking {
    private int id;
    private Guest guest;
    private Room room;
    private Date startDate;
    private Date endDate;

    public Booking(int id, Guest guest, Room room, Date startDate, Date endDate) {
        this.id = id;
        this.guest = guest;
        this.room = room;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    // Getters and Setters
}