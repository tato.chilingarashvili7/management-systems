import java.util.ArrayList;
import java.util.List;

public class Guest {
    private int id;
    private String name;
    private String contactInfo;
    private List<Booking> bookings;

    public Guest(int id, String name, String contactInfo) {
        this.id = id;
        this.name = name;
        this.contactInfo = contactInfo;
        this.bookings = new ArrayList<>();
    }

    // Getters and Setters
}
