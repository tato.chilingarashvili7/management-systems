import java.util.Date;

public class FeedingSchedule {
    private int id;
    private Animal animal;
    private Food food;
    private Date feedingTime;

    public FeedingSchedule(int id, Animal animal, Food food, Date feedingTime) {
        this.id = id;
        this.animal = animal;
        this.food = food;
        this.feedingTime = feedingTime;
    }

    // Getters and Setters
}
