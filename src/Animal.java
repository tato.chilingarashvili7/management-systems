import java.util.ArrayList;
import java.util.List;
public class Animal {
    private int id;
    private String name;
    private String species;
    private int age;
    private Zoo zoo;
    private List<FeedingSchedule> feedingSchedules;

    public Animal(int id, String name, String species, int age, Zoo zoo) {
        this.id = id;
        this.name = name;
        this.species = species;
        this.age = age;
        this.zoo = zoo;
        this.feedingSchedules = new ArrayList<>();
    }

    // Getters and Setters
}
