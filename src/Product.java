public class Product {
    private int id;
    private String name;
    private double price;
    private int quantity;
    private Warehouse warehouse;

    public Product(int id, String name, double price, int quantity, Warehouse warehouse) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.warehouse = warehouse;
    }

    // Getters and Setters
}
