import java.util.ArrayList;
import java.util.List;
public class Food {
    private int id;
    private String name;
    private String nutritionalValue;
    private List<FeedingSchedule> feedingSchedules;

    public Food(int id, String name, String nutritionalValue) {
        this.id = id;
        this.name = name;
        this.nutritionalValue = nutritionalValue;
        this.feedingSchedules = new ArrayList<>();
    }

    // Getters and Setters
}