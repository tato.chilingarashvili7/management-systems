import java.util.ArrayList;
import java.util.List;

public class Zoo implements ManagementSystem {
    private int id;
    private String name;
    private String location;
    private List<Animal> animals;

    public Zoo(int id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.animals = new ArrayList<>();
    }

    public void addAnimal() {
        // Implementation
    }

    public void removeAnimal() {
        // Implementation
    }

    @Override
    public void manage() {

    }

    // Getters and Setters
}
