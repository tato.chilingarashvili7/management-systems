public class Room {
    private int id;
    private int roomNumber;
    private int capacity;
    private boolean isAvailable;
    private Dormitory dormitory;

    public Room(int id, int roomNumber, int capacity, boolean isAvailable, Dormitory dormitory) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.isAvailable = isAvailable;
        this.dormitory = dormitory;
    }

    public void reserve() {
        // Implementation
    }

    public void release() {
        // Implementation
    }

    // Getters and Setters
}
