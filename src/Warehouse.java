import java.util.ArrayList;
import java.util.List;

public class Warehouse implements ManagementSystem {
    private int id;
    private String name;
    private String location;
    private List<Product> products;

    public Warehouse(int id, String name, String location) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.products = new ArrayList<>();
    }

    public void addProduct() {
        // Implementation
    }

    public void removeProduct() {
        // Implementation
    }

    @Override
    public void manage() {

    }

    // Getters and Setters
}
