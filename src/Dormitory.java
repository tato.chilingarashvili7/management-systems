import java.util.ArrayList;
import java.util.List;
public class Dormitory implements ManagementSystem {
    private int id;
    private String name;
    private String location;
    private int capacity;
    private List<Room> rooms;

    public Dormitory(int id, String name, String location, int capacity) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.capacity = capacity;
        this.rooms = new ArrayList<>();
    }

    public void checkAvailability() {
        // Implementation
    }

    public void bookRoom() {
        // Implementation
    }

    public void cancelBooking() {
        // Implementation
    }

    @Override
    public void manage() {

    }

    // Getters and Setters
}